/**
 *
 * InertialStateEstimator.hpp: ...
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created Jun 04, 2013
 * @modified Jun 04, 2013
 *
 */

#pragma once

#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include <boost/shared_ptr.hpp>
#include <atlas_msgs/AtlasState.h>

namespace re2uta
{

namespace estimate
{

/*
 * This class will implement a Discrete-Time Kalman Filter as described in Table 2.1 pg 72 of
 * Optimal and Robust Estimation with an Introduction to Stochastic Control Theory, Second Edition by Lewis, F.L., et al; 2008
 * TODO add more description
 */
class InertialStateEstimator
{
  private:
            Eigen::Vector3d m_linearPos;
            Eigen::Vector3d m_linearVel;
            Eigen::Vector3d m_linearAcc;

            Eigen::Vector3d m_angularPos;
            Eigen::Vector3d m_angularVel;
            Eigen::Vector3d m_angularAcc;

  public:
            typedef boost::shared_ptr<InertialStateEstimator> Ptr;

            InertialStateEstimator( )
            {

              m_linearPos  = Eigen::Vector3d::Zero();
              m_linearVel  = Eigen::Vector3d::Zero();
              m_linearAcc  = Eigen::Vector3d::Zero();

              m_angularPos = Eigen::Vector3d::Zero();
              m_angularVel = Eigen::Vector3d::Zero();
              m_angularAcc = Eigen::Vector3d::Zero();

            }

            ~InertialStateEstimator()
            {

            }

            void MeasurementUpdate( const atlas_msgs::AtlasState & state );

            Eigen::Vector3d getLinearPosEstimate();
            Eigen::Vector3d getLinearVelEstimate();
            Eigen::Vector3d getLinearAccEstimate();

            Eigen::Vector3d getAngularPosEstimate();
            Eigen::Vector3d getAngularVelEstimate();
            Eigen::Vector3d getAngularAccEstimate();

            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}

}
