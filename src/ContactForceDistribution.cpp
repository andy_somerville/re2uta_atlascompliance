/*
 * ContactForceDistribution.cpp
 *
 *  Created on: Jun 12, 2013
 *      Author: Isura Ranatunga
 */

#include <re2uta/ContactForceDistribution.hpp>

#include <atlas_msgs/SetJointDamping.h>

#include <re2uta/AtlasLookup.hpp>
#include <atlas_msgs/AtlasState.h>
#include <atlas_msgs/AtlasCommand.h>
#include <re2uta/AtlasLookup.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2/eigen/eigen_util.h>

#include <ros/ros.h>
#include <string>


namespace re2uta
{


ContactForceDistribution::
ContactForceDistribution()
{
    //Position of contact points wrt to foot frame
    m_rContactPoint1 = Eigen::Vector3d(  0.184f,  0.0624435f, -0.079342f );
    m_rContactPoint2 = Eigen::Vector3d(  0.184f, -0.0624435f, -0.079342f );
    m_rContactPoint3 = Eigen::Vector3d( -0.076f, -0.0624435f, -0.079342f );
    m_rContactPoint4 = Eigen::Vector3d( -0.076f,  0.0624435f, -0.079342f );

    // TODO mirror for left
    m_lContactPoint1 = Eigen::Vector3d(  0.184f,  0.0624435f, -0.079342f );
    m_lContactPoint2 = Eigen::Vector3d(  0.184f, -0.0624435f, -0.079342f );
    m_lContactPoint3 = Eigen::Vector3d( -0.076f, -0.0624435f, -0.079342f );
    m_lContactPoint4 = Eigen::Vector3d( -0.076f,  0.0624435f, -0.079342f );

    m_rContactPoint1InBase = Eigen::Vector3d::Zero();
    m_rContactPoint2InBase = Eigen::Vector3d::Zero();
    m_rContactPoint3InBase = Eigen::Vector3d::Zero();
    m_rContactPoint4InBase = Eigen::Vector3d::Zero();

    m_lContactPoint1InBase = Eigen::Vector3d::Zero();
    m_lContactPoint2InBase = Eigen::Vector3d::Zero();
    m_lContactPoint3InBase = Eigen::Vector3d::Zero();
    m_lContactPoint4InBase = Eigen::Vector3d::Zero();

    m_support_mode = SUPPORT_DOUBLE;

}

geometry_msgs::Point pointEigen2Msg( Eigen::Vector3d point )
{

  geometry_msgs::Point pointMsg;

  pointMsg.x = point( 0 );
  pointMsg.y = point( 1 );
  pointMsg.z = point( 2 );

  return pointMsg;
}

Eigen::MatrixXd reorderContactForceVector2Mat( Eigen::VectorXd & desACF_fS )
{

    int rows = desACF_fS.rows();
    int contactPts = rows/3;

    Eigen::MatrixXd contactForces= Eigen::MatrixXd::Zero( contactPts, 3 );

    contactForces.col(0) = desACF_fS.segment(            0, contactPts );
    contactForces.col(1) = desACF_fS.segment(   contactPts, contactPts );
    contactForces.col(2) = desACF_fS.segment( 2*contactPts, contactPts );

    return contactForces;

}

Eigen::VectorXd
ContactForceDistribution::
reorderContactForceVector2Vec( Eigen::VectorXd & desACF_fS )
{
    Eigen::MatrixXd mat = reorderContactForceVector2Mat( desACF_fS ).transpose();
    return Eigen::VectorXd::Map( mat.data(), mat.rows()*mat.cols() );
}

Eigen::Vector3d com2ContactPointVector( Eigen::Vector3d & com, Eigen::Vector3d & contactPoint )
{
 return  (contactPoint - com);
}

Eigen::MatrixXd pseudoInverse( Eigen::MatrixXd & A )
{
  Eigen::MatrixXd A_Atrans = A * A.transpose();
  return A.transpose()*A_Atrans.inverse();
}








visualization_msgs::Marker
ContactForceDistribution::
getCom2ContactLineMarker( std::string baseName, Eigen::Vector3d & comPosition )
{
    // Build and publish COM marker
    visualization_msgs::Marker com2contactLine;
    com2contactLine.header.stamp = ros::Time::now();
    com2contactLine.header.frame_id = baseName;

    if( m_support_mode == SUPPORT_SINGLE_LEFT )
    {
      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint1InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint2InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint3InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint4InBase ) );

    }else if( m_support_mode == SUPPORT_SINGLE_RIGHT )
    {
      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint1InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint2InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint3InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint4InBase ) );

    }else
    {
      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint1InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint2InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint3InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint4InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint1InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint2InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint3InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( comPosition            ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint4InBase ) );

    }

    com2contactLine.action = visualization_msgs::Marker::ADD;
    com2contactLine.type = visualization_msgs::Marker::LINE_LIST;
;
    com2contactLine.scale.x = 0.002;
    com2contactLine.scale.y = 0.002;
//                    com2contactLine.scale.z = 0.01;

    com2contactLine.color.a = 1.0;
    com2contactLine.color.b = 0.0;
    com2contactLine.color.g = 1.0;
    com2contactLine.color.r = 1.0;

    return com2contactLine;
}


visualization_msgs::Marker
ContactForceDistribution::
getContactForceLineMarker( std::string baseName, Eigen::VectorXd & desACF_fS )
{
    // Build and publish COM marker
    visualization_msgs::Marker com2contactLine;
    com2contactLine.header.stamp = ros::Time::now();
    com2contactLine.header.frame_id = baseName;

    if( m_support_mode == SUPPORT_SINGLE_LEFT )
    {
      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_lContactPoint1InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment(  0, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint1InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_lContactPoint2InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment(  3, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint2InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_lContactPoint3InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment(  6, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint3InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_lContactPoint4InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment(  9, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint4InBase ) );

    }else if( m_support_mode == SUPPORT_SINGLE_RIGHT )
    {
      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_rContactPoint1InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment(  0, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint1InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_rContactPoint2InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment(  3, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint2InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_rContactPoint3InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment(  6, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint3InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_rContactPoint4InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment(  9, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint4InBase ) );

    }else
    {
      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_lContactPoint1InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment(  0, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint1InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_lContactPoint2InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment(  3, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint2InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_lContactPoint3InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment(  6, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint3InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_lContactPoint4InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment(  9, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_lContactPoint4InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_rContactPoint1InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment( 12, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint1InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_rContactPoint2InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment( 15, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint2InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_rContactPoint3InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment( 18, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint3InBase ) );

      com2contactLine.points.push_back( pointEigen2Msg( Eigen::Vector3d( m_rContactPoint4InBase +  Eigen::Vector3d( 0, 0, desACF_fS.segment( 21, 3)(2) ) ) ) );
      com2contactLine.points.push_back( pointEigen2Msg( m_rContactPoint4InBase ) );

    }

    std::cout << std::endl << std::endl << desACF_fS << std::endl;

    com2contactLine.action = visualization_msgs::Marker::ADD;
    com2contactLine.type = visualization_msgs::Marker::LINE_LIST;
;
    com2contactLine.scale.x = 0.002;
    com2contactLine.scale.y = 0.002;
//                    com2contactLine.scale.z = 0.01;

    com2contactLine.color.a = 1.0;
    com2contactLine.color.b = 0.0;
    com2contactLine.color.g = 0.0;
    com2contactLine.color.r = 1.0;

    return com2contactLine;
}

Eigen::MatrixXd
ContactForceDistribution::
calculateZContactForceDistributionMatrix( Eigen::Vector3d & comPosition )
{

  Eigen::MatrixXd Az;


  if( m_support_mode == SUPPORT_SINGLE_LEFT )
  {
    Az = Eigen::MatrixXd::Ones( 3, 4 );

    Eigen::Vector3d lCom2ContactPoint1InBase = com2ContactPointVector( comPosition, m_lContactPoint1InBase );
    Eigen::Vector3d lCom2ContactPoint2InBase = com2ContactPointVector( comPosition, m_lContactPoint2InBase );
    Eigen::Vector3d lCom2ContactPoint3InBase = com2ContactPointVector( comPosition, m_lContactPoint3InBase );
    Eigen::Vector3d lCom2ContactPoint4InBase = com2ContactPointVector( comPosition, m_lContactPoint4InBase );

    Az( 0, 0) = lCom2ContactPoint1InBase( 0 );
    Az( 1, 0) = lCom2ContactPoint1InBase( 1 );
    Az( 0, 1) = lCom2ContactPoint2InBase( 0 );
    Az( 1, 1) = lCom2ContactPoint2InBase( 1 );
    Az( 0, 2) = lCom2ContactPoint3InBase( 0 );
    Az( 1, 2) = lCom2ContactPoint3InBase( 1 );
    Az( 0, 3) = lCom2ContactPoint4InBase( 0 );
    Az( 1, 3) = lCom2ContactPoint4InBase( 1 );

  }else if( m_support_mode == SUPPORT_SINGLE_RIGHT )
  {

    Az = Eigen::MatrixXd::Ones( 3, 4 );

    Eigen::Vector3d rCom2ContactPoint1InBase = com2ContactPointVector( comPosition, m_rContactPoint1InBase );
    Eigen::Vector3d rCom2ContactPoint2InBase = com2ContactPointVector( comPosition, m_rContactPoint2InBase );
    Eigen::Vector3d rCom2ContactPoint3InBase = com2ContactPointVector( comPosition, m_rContactPoint3InBase );
    Eigen::Vector3d rCom2ContactPoint4InBase = com2ContactPointVector( comPosition, m_rContactPoint4InBase );

    Az( 0, 0) = rCom2ContactPoint1InBase( 0 );
    Az( 1, 0) = rCom2ContactPoint1InBase( 1 );
    Az( 0, 1) = rCom2ContactPoint2InBase( 0 );
    Az( 1, 1) = rCom2ContactPoint2InBase( 1 );
    Az( 0, 2) = rCom2ContactPoint3InBase( 0 );
    Az( 1, 2) = rCom2ContactPoint3InBase( 1 );
    Az( 0, 3) = rCom2ContactPoint4InBase( 0 );
    Az( 1, 3) = rCom2ContactPoint4InBase( 1 );

  }else
  {

    Az = Eigen::MatrixXd::Ones( 3, 8 );

    Eigen::Vector3d lCom2ContactPoint1InBase = com2ContactPointVector( comPosition, m_lContactPoint1InBase );
    Eigen::Vector3d lCom2ContactPoint2InBase = com2ContactPointVector( comPosition, m_lContactPoint2InBase );
    Eigen::Vector3d lCom2ContactPoint3InBase = com2ContactPointVector( comPosition, m_lContactPoint3InBase );
    Eigen::Vector3d lCom2ContactPoint4InBase = com2ContactPointVector( comPosition, m_lContactPoint4InBase );

    Eigen::Vector3d rCom2ContactPoint1InBase = com2ContactPointVector( comPosition, m_rContactPoint1InBase );
    Eigen::Vector3d rCom2ContactPoint2InBase = com2ContactPointVector( comPosition, m_rContactPoint2InBase );
    Eigen::Vector3d rCom2ContactPoint3InBase = com2ContactPointVector( comPosition, m_rContactPoint3InBase );
    Eigen::Vector3d rCom2ContactPoint4InBase = com2ContactPointVector( comPosition, m_rContactPoint4InBase );

    Az( 0, 0) = lCom2ContactPoint1InBase( 0 );
    Az( 1, 0) = lCom2ContactPoint1InBase( 1 );
    Az( 0, 1) = lCom2ContactPoint2InBase( 0 );
    Az( 1, 1) = lCom2ContactPoint2InBase( 1 );
    Az( 0, 2) = lCom2ContactPoint3InBase( 0 );
    Az( 1, 2) = lCom2ContactPoint3InBase( 1 );
    Az( 0, 3) = lCom2ContactPoint4InBase( 0 );
    Az( 1, 3) = lCom2ContactPoint4InBase( 1 );

    Az( 0, 4) = rCom2ContactPoint1InBase( 0 );
    Az( 1, 4) = rCom2ContactPoint1InBase( 1 );
    Az( 0, 5) = rCom2ContactPoint2InBase( 0 );
    Az( 1, 5) = rCom2ContactPoint2InBase( 1 );
    Az( 0, 6) = rCom2ContactPoint3InBase( 0 );
    Az( 1, 6) = rCom2ContactPoint3InBase( 1 );
    Az( 0, 7) = rCom2ContactPoint4InBase( 0 );
    Az( 1, 7) = rCom2ContactPoint4InBase( 1 );

  }

  return Az;

}

Eigen::MatrixXd
ContactForceDistribution::
calculateYContactForceDistributionMatrix( Eigen::Vector3d & comPosition )
{

  Eigen::MatrixXd Ay;


  if( m_support_mode == SUPPORT_SINGLE_LEFT )
  {

    Ay = Eigen::MatrixXd::Ones( 2, 4 );

    Eigen::Vector3d lCom2ContactPoint1InBase = com2ContactPointVector( comPosition, m_lContactPoint1InBase );
    Eigen::Vector3d lCom2ContactPoint2InBase = com2ContactPointVector( comPosition, m_lContactPoint2InBase );
    Eigen::Vector3d lCom2ContactPoint3InBase = com2ContactPointVector( comPosition, m_lContactPoint3InBase );
    Eigen::Vector3d lCom2ContactPoint4InBase = com2ContactPointVector( comPosition, m_lContactPoint4InBase );

    Ay( 0, 0) = lCom2ContactPoint1InBase( 1 );
    Ay( 0, 1) = lCom2ContactPoint2InBase( 1 );
    Ay( 0, 2) = lCom2ContactPoint3InBase( 1 );
    Ay( 0, 3) = lCom2ContactPoint4InBase( 1 );

  }else if( m_support_mode == SUPPORT_SINGLE_RIGHT )
  {

    Ay = Eigen::MatrixXd::Ones( 2, 4 );

    Eigen::Vector3d rCom2ContactPoint1InBase = com2ContactPointVector( comPosition, m_rContactPoint1InBase );
    Eigen::Vector3d rCom2ContactPoint2InBase = com2ContactPointVector( comPosition, m_rContactPoint2InBase );
    Eigen::Vector3d rCom2ContactPoint3InBase = com2ContactPointVector( comPosition, m_rContactPoint3InBase );
    Eigen::Vector3d rCom2ContactPoint4InBase = com2ContactPointVector( comPosition, m_rContactPoint4InBase );

    Ay( 0, 0) = rCom2ContactPoint1InBase( 1 );
    Ay( 0, 1) = rCom2ContactPoint2InBase( 1 );
    Ay( 0, 2) = rCom2ContactPoint3InBase( 1 );
    Ay( 0, 3) = rCom2ContactPoint4InBase( 1 );

  }else
  {

    Ay = Eigen::MatrixXd::Ones( 2, 8 );

    Eigen::Vector3d lCom2ContactPoint1InBase = com2ContactPointVector( comPosition, m_lContactPoint1InBase );
    Eigen::Vector3d lCom2ContactPoint2InBase = com2ContactPointVector( comPosition, m_lContactPoint2InBase );
    Eigen::Vector3d lCom2ContactPoint3InBase = com2ContactPointVector( comPosition, m_lContactPoint3InBase );
    Eigen::Vector3d lCom2ContactPoint4InBase = com2ContactPointVector( comPosition, m_lContactPoint4InBase );

    Eigen::Vector3d rCom2ContactPoint1InBase = com2ContactPointVector( comPosition, m_rContactPoint1InBase );
    Eigen::Vector3d rCom2ContactPoint2InBase = com2ContactPointVector( comPosition, m_rContactPoint2InBase );
    Eigen::Vector3d rCom2ContactPoint3InBase = com2ContactPointVector( comPosition, m_rContactPoint3InBase );
    Eigen::Vector3d rCom2ContactPoint4InBase = com2ContactPointVector( comPosition, m_rContactPoint4InBase );

    Ay( 0, 0) = lCom2ContactPoint1InBase( 1 );
    Ay( 0, 1) = lCom2ContactPoint2InBase( 1 );
    Ay( 0, 2) = lCom2ContactPoint3InBase( 1 );
    Ay( 0, 3) = lCom2ContactPoint4InBase( 1 );

    Ay( 0, 4) = rCom2ContactPoint1InBase( 1 );
    Ay( 0, 5) = rCom2ContactPoint2InBase( 1 );
    Ay( 0, 6) = rCom2ContactPoint3InBase( 1 );
    Ay( 0, 7) = rCom2ContactPoint4InBase( 1 );

  }

  return Ay;

}

Eigen::MatrixXd
ContactForceDistribution::
calculateXContactForceDistributionMatrix( Eigen::Vector3d & comPosition )
{

  Eigen::MatrixXd Ax;

  if( m_support_mode == SUPPORT_SINGLE_LEFT )
  {

    Ax = Eigen::MatrixXd::Ones( 2, 4 );

    Eigen::Vector3d lCom2ContactPoint1InBase = com2ContactPointVector( comPosition, m_lContactPoint1InBase );
    Eigen::Vector3d lCom2ContactPoint2InBase = com2ContactPointVector( comPosition, m_lContactPoint2InBase );
    Eigen::Vector3d lCom2ContactPoint3InBase = com2ContactPointVector( comPosition, m_lContactPoint3InBase );
    Eigen::Vector3d lCom2ContactPoint4InBase = com2ContactPointVector( comPosition, m_lContactPoint4InBase );

    Ax( 0, 0) = lCom2ContactPoint1InBase( 0 );
    Ax( 0, 1) = lCom2ContactPoint2InBase( 0 );
    Ax( 0, 2) = lCom2ContactPoint3InBase( 0 );
    Ax( 0, 3) = lCom2ContactPoint4InBase( 0 );

  }else if( m_support_mode == SUPPORT_SINGLE_RIGHT )
  {

    Ax = Eigen::MatrixXd::Ones( 2, 4 );

    Eigen::Vector3d rCom2ContactPoint1InBase = com2ContactPointVector( comPosition, m_rContactPoint1InBase );
    Eigen::Vector3d rCom2ContactPoint2InBase = com2ContactPointVector( comPosition, m_rContactPoint2InBase );
    Eigen::Vector3d rCom2ContactPoint3InBase = com2ContactPointVector( comPosition, m_rContactPoint3InBase );
    Eigen::Vector3d rCom2ContactPoint4InBase = com2ContactPointVector( comPosition, m_rContactPoint4InBase );

    Ax( 0, 0) = rCom2ContactPoint1InBase( 0 );
    Ax( 0, 1) = rCom2ContactPoint2InBase( 0 );
    Ax( 0, 2) = rCom2ContactPoint3InBase( 0 );
    Ax( 0, 3) = rCom2ContactPoint4InBase( 0 );

  }else
  {

    Ax = Eigen::MatrixXd::Ones( 2, 8 );

    Eigen::Vector3d lCom2ContactPoint1InBase = com2ContactPointVector( comPosition, m_lContactPoint1InBase );
    Eigen::Vector3d lCom2ContactPoint2InBase = com2ContactPointVector( comPosition, m_lContactPoint2InBase );
    Eigen::Vector3d lCom2ContactPoint3InBase = com2ContactPointVector( comPosition, m_lContactPoint3InBase );
    Eigen::Vector3d lCom2ContactPoint4InBase = com2ContactPointVector( comPosition, m_lContactPoint4InBase );

    Eigen::Vector3d rCom2ContactPoint1InBase = com2ContactPointVector( comPosition, m_rContactPoint1InBase );
    Eigen::Vector3d rCom2ContactPoint2InBase = com2ContactPointVector( comPosition, m_rContactPoint2InBase );
    Eigen::Vector3d rCom2ContactPoint3InBase = com2ContactPointVector( comPosition, m_rContactPoint3InBase );
    Eigen::Vector3d rCom2ContactPoint4InBase = com2ContactPointVector( comPosition, m_rContactPoint4InBase );

    Ax( 0, 0) = lCom2ContactPoint1InBase( 0 );
    Ax( 0, 1) = lCom2ContactPoint2InBase( 0 );
    Ax( 0, 2) = lCom2ContactPoint3InBase( 0 );
    Ax( 0, 3) = lCom2ContactPoint4InBase( 0 );

    Ax( 0, 4) = rCom2ContactPoint1InBase( 0 );
    Ax( 0, 5) = rCom2ContactPoint2InBase( 0 );
    Ax( 0, 6) = rCom2ContactPoint3InBase( 0 );
    Ax( 0, 7) = rCom2ContactPoint4InBase( 0 );

  }

  return Ax;

}

Eigen::MatrixXd
ContactForceDistribution::
calculateFullContactForceDistributionMatrix( Eigen::Vector3d & comPosition )
{

  Eigen::MatrixXd Ax = calculateXContactForceDistributionMatrix( comPosition );
  Eigen::MatrixXd Ay = calculateYContactForceDistributionMatrix( comPosition );
  Eigen::MatrixXd Az = calculateZContactForceDistributionMatrix( comPosition );

  Eigen::MatrixXd A = Eigen::MatrixXd::Zero( Ax.rows() + Ay.rows() + Az.rows(), Ax.cols() + Ay.cols() + Az.cols() );

  A.block(                     0,                     0, Ax.rows(), Ax.cols() ) = Ax;
  A.block(             Ax.rows(),             Ax.cols(), Ay.rows(), Ay.cols() ) = Ay;
  A.block( Ax.rows() + Ay.rows(), Ax.cols() + Ay.cols(), Az.rows(), Az.cols() ) = Az;

  return A;

}





void
ContactForceDistribution::
setSupportMode( FootSupport & support_mode )
{
    m_support_mode = support_mode;
}

void
ContactForceDistribution::
setlContactPointInBase( Eigen::Affine3d & l_footPos )
{
    m_lContactPoint1InBase = l_footPos*m_lContactPoint1;
    m_lContactPoint3InBase = l_footPos*m_lContactPoint3;
    m_lContactPoint4InBase = l_footPos*m_lContactPoint4;
}

void
ContactForceDistribution::
setrContactPointInBase( Eigen::Affine3d & r_footPos )
{
    m_rContactPoint1InBase = r_footPos*m_rContactPoint1;
    m_rContactPoint2InBase = r_footPos*m_rContactPoint2;
    m_rContactPoint3InBase = r_footPos*m_rContactPoint3;
    m_rContactPoint4InBase = r_footPos*m_rContactPoint4;
}



Eigen::MatrixXd
ContactForceDistribution::
getA(     Eigen::Vector3d & comPosition )
{
    return calculateFullContactForceDistributionMatrix( comPosition );
}

Eigen::MatrixXd
ContactForceDistribution::
getPinvA( Eigen::Vector3d & comPosition )
{
    Eigen::MatrixXd A = calculateFullContactForceDistributionMatrix( comPosition );
    return pseudoInverse( A );
}


Eigen::Vector3d
ContactForceDistribution::
getlContactPoint1( )
{
    return m_lContactPoint1;
}

Eigen::Vector3d
ContactForceDistribution::
getlContactPoint2( )
{
    return m_lContactPoint2;
}

Eigen::Vector3d
ContactForceDistribution::
getlContactPoint3( )
{
    return m_lContactPoint3;
}

Eigen::Vector3d
ContactForceDistribution::
getlContactPoint4( )
{
    return m_lContactPoint4;
}





Eigen::Vector3d
ContactForceDistribution::
getrContactPoint1( )
{
    return m_rContactPoint1;
}

Eigen::Vector3d
ContactForceDistribution::
getrContactPoint2( )
{
    return m_rContactPoint2;
}

Eigen::Vector3d
ContactForceDistribution::
getrContactPoint3( )
{
    return m_rContactPoint3;
}

Eigen::Vector3d
ContactForceDistribution::
getrContactPoint4( )
{
    return m_rContactPoint4;
}





Eigen::Vector3d
ContactForceDistribution::
getlContactPoint1InBase( )
{
    return m_lContactPoint1InBase;
}

Eigen::Vector3d
ContactForceDistribution::
getlContactPoint2InBase( )
{
    return m_lContactPoint2InBase;
}

Eigen::Vector3d
ContactForceDistribution::
getlContactPoint3InBase( )
{
    return m_lContactPoint3InBase;
}

Eigen::Vector3d
ContactForceDistribution::
getlContactPoint4InBase( )
{
    return m_lContactPoint4InBase;
}





Eigen::Vector3d
ContactForceDistribution::
getrContactPoint1InBase( )
{
    return m_rContactPoint1InBase;
}

Eigen::Vector3d
ContactForceDistribution::
getrContactPoint2InBase( )
{
    return m_rContactPoint2InBase;
}

Eigen::Vector3d
ContactForceDistribution::
getrContactPoint3InBase( )
{
    return m_rContactPoint3InBase;
}

Eigen::Vector3d
ContactForceDistribution::
getrContactPoint4InBase( )
{
    return m_rContactPoint4InBase;
}




}
