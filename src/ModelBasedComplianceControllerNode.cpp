/*
 * ModelBasedComplianceControllerNode.cpp
 *
 *  Created on: June 13, 2013
 *      Author: andrew.somerville
 *              Isura Ranatunga
 */


#include <re2uta/ModelBasedComplianceController.hpp>
#include <atlas_msgs/SetJointDamping.h>

#include <re2uta/AtlasLookup.hpp>
#include <atlas_msgs/AtlasState.h>
#include <atlas_msgs/AtlasCommand.h>
#include <re2uta/AtlasLookup.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2/eigen/eigen_util.h>

#include <ros/ros.h>
#include <string>


//// FIXME remove or move the dynamic config stuff to trunk
//#include <dynamic_reconfigure/server.h>
//#include <uta_drc_robotControl/controllerParamsConfig.h>

using namespace re2uta;

class ModelBasedComplianceControllerNode
{
    public:
        ModelBasedComplianceControllerNode()
        {
            m_controllerPeriod = ros::Duration( 0.002 );

            std::string urdfString;
            m_node.getParam( "/robot_description", urdfString );

            urdf::Model urdfModel;
            urdfModel.initString( urdfString );

            m_atlasLookup.reset( new re2uta::AtlasLookup( urdfModel ) );
            m_timerBool = false;

            ros::SubscribeOptions atlasStateOptions;
            atlasStateOptions = ros::SubscribeOptions::create<atlas_msgs::AtlasState>( "/atlas/atlas_state", 1,
                                                               boost::bind( &ModelBasedComplianceControllerNode::handleAtlasState, this, _1 ),
                                                               ros::VoidPtr(),
                                                               m_node.getCallbackQueue() );

            atlasStateOptions.transport_hints = ros::TransportHints().tcpNoDelay( true );

            m_atlasStateSub   = m_node.subscribe( atlasStateOptions );
            m_atlasCommandSub = m_node.subscribe( "/walk/atlas_command", 1, &ModelBasedComplianceControllerNode::handleAtlasCommand, this );
            m_atlasCommandPub = m_node.advertise<atlas_msgs::AtlasCommand>( "/atlas/atlas_command", 1 );
            m_commandTimer    = m_node.createTimer( m_controllerPeriod, &ModelBasedComplianceControllerNode::sendCommand, this ); //, false, false);


//            m_controller.setfilterForGain( 500 );
//
//            m_controller.setfilterPosGain( 10 );
//            m_controller.setfilterVelGain( 10 );


//            m_setDampingSrv   = m_node.serviceClient<atlas_msgs::SetJointDamping>( "/atlas/set_joint_damping" );
//
//            m_setDampingSrv.waitForExistence();
//
//            atlas_msgs::SetJointDamping transaction;
//            transaction.request.damping_coefficients.fill( 10 );
//            m_setDampingSrv.call( transaction );
        }

//        void dynamicReconfigureCallback( uta_drc_robotControl::controllerParamsConfig &config, uint32_t level )
//        {
//          ROS_INFO("Reconfigure Request | FT CUttoff: %d | Kp: %f | Kv: %f",
//                    config.force_filter,
//                    config.kp_gain,
//                    config.kv_gain);
//
//        //  KfilterPos = config.position_filter;;
//        //  KfilterVel = config.velocity_filter;;
//
//          m_controller.setfilterForGain( (double)config.force_filter );
//
//          m_controller.setfilterForGain( (double)config.kp_gain );
//          m_controller.setfilterForGain( (double)config.kv_gain );
//        }

        void go()
        {
//            // Dynamic reconfigure stuff
//            dynamic_reconfigure::Server<uta_drc_robotControl::controllerParamsConfig> server;
//            dynamic_reconfigure::Server<uta_drc_robotControl::controllerParamsConfig>::CallbackType f;
//
//            f = boost::bind( dynamicReconfigureCallback, _1, _2 );
//            server.setCallback( f );
//            // End Dynamic reconfigure stuff

            ros::spin();
        }


    protected:

        void handleAtlasState( const atlas_msgs::AtlasStateConstPtr & msg )
        {
            if( !m_timerBool )
            {
                m_commandTimer.start();
                m_timerBool = true;
            }

            m_lastAtlasState = msg;

        }

        void handleAtlasCommand( const atlas_msgs::AtlasCommandConstPtr & msg )
        {
            m_lastAtlasCommand = msg;
        }

        void sendCommand( const ros::TimerEvent & event )
        {
            Eigen::VectorXd torques;

            m_controller.setStateMsg(   m_lastAtlasState   );
            m_controller.setCommandMsg( m_lastAtlasCommand );
            torques = m_controller.calcTorques( m_controllerPeriod.toSec() );

            if( torques.size() == 0 )
                return;

            atlas_msgs::AtlasCommand::Ptr atlasCommandMsg;

            atlasCommandMsg = m_atlasLookup->createEmptyJointCmdMsg();
            atlasCommandMsg->k_effort = std::vector<uint8_t>( m_atlasLookup->getNumCmdJoints(), 255 );
            atlasCommandMsg->effort   = re2::toStd( torques );
            atlasCommandMsg->desired_controller_period_ms = 5;

            m_atlasCommandPub.publish( atlasCommandMsg );
        }


    protected:
        ros::NodeHandle          m_node;
        ros::ServiceClient       m_setDampingSrv;
        re2uta::AtlasLookup::Ptr m_atlasLookup;
        ros::Duration            m_controllerPeriod;
        ros::Timer               m_commandTimer;
        ros::Subscriber          m_atlasStateSub;
        ros::Subscriber          m_atlasCommandSub;
        ros::Publisher           m_atlasCommandPub;
        atlas_msgs::AtlasStateConstPtr    m_lastAtlasState;
        atlas_msgs::AtlasCommandConstPtr  m_lastAtlasCommand;
        ModelBasedComplianceController    m_controller;
        bool                     m_timerBool;

};



int main(int argc, char** argv)
{
    ros::init(argc, argv, "rbdl_model_compliance_control_node");

    ros::NodeHandle node;

    std::string urdfString;
    node.getParam( "/robot_description", urdfString );


    ModelBasedComplianceControllerNode modelBasedComplianceControllerNode;

    modelBasedComplianceControllerNode.go();

    return 0;
}
